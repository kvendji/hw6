from core.models import Group, Student, Teacher

from django.contrib import admin


class StudentAdmin(admin.ModelAdmin):
    list_display = ('name', 'last_name', 'phone', 'age', 'group')
    search_fields = ('last_name',)
    list_filter = ('group', )


admin.site.register(Teacher)
admin.site.register(Group)
admin.site.register(Student, StudentAdmin)
