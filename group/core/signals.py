from django.db.models.signals import pre_save
from core.models import Teacher, Student


def capitalize_signal(sender, instance, **kwargs):
    instance.name = instance.name.capitalize()
    instance.last_name = instance.last_name.capitalize()


pre_save.connect(capitalize_signal, sender=Teacher)
pre_save.connect(capitalize_signal, sender=Student)
