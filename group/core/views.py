from core.forms import GroupCreateForm, StudentForm
from core.models import Group, Student, Teacher


from django.db.models.aggregates import Avg, Count, Max, Min
from django.urls import reverse_lazy
from django.views.generic import CreateView,\
                                 DeleteView, \
                                 TemplateView,\
                                 UpdateView


class GroupView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        info_about_group = Group.objects.all().select_related().annotate(
            student_count=Count('student'),
            avg_age=Avg('student__age'),
            min_age=Min('student__age'),
            max_age=Max('student__age')
        )
        context = {
            'info_about_group': info_about_group
        }
        return context


class CreateGroupView(CreateView):
    template_name = 'create.html'
    form_class = GroupCreateForm
    success_url = reverse_lazy('core:home')


class StudentView(TemplateView):
    template_name = 'students_list.html'

    def get_context_data(self, **kwargs):
        students = Student.objects.all().select_related()
        context = {
            'students': students
        }
        return context


class UpdateStudentView(UpdateView):
    template_name = 'create.html'
    success_url = reverse_lazy('core:home')
    form_class = StudentForm
    model = Student


class CreateStudentView(CreateView):
    template_name = 'create.html'
    success_url = reverse_lazy('core:home')
    form_class = StudentForm


class DeleteStudentView(DeleteView):
    model = Student
    success_url = reverse_lazy('core:home')

    def get_success_url(self):
        return reverse_lazy('core:home')


class TeacherView(TemplateView):
    template_name = 'teacher_list.html'

    def get_context_data(self, **kwargs):
        teachers = Teacher.objects.all().select_related()
        context = {
            'teachers': teachers
        }
        return context


class CreateTeacherView(CreateView):
    template_name = 'create.html'
    success_url = reverse_lazy('core:home')
    model = Teacher
    fields = '__all__'


class UpdateTeacherView(UpdateView):
    template_name = 'create.html'
    success_url = reverse_lazy('core:home')
    model = Teacher
    fields = '__all__'


class DeleteTeacherView(DeleteView):
    model = Teacher
    success_url = reverse_lazy('core:home')

    def get_success_url(self):
        return reverse_lazy('core:home')
