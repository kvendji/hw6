from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Teacher(models.Model):
    name = models.CharField(max_length=255, verbose_name='Имя')
    last_name = models.CharField(max_length=255, verbose_name='Фамилия')

    class Meta:
        verbose_name = 'Препод'
        verbose_name_plural = 'Преподы'

    def __str__(self):
        return f"{self.name} {self.last_name}"


class Group(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название')
    teacher = models.ForeignKey(Teacher,
                                on_delete=models.CASCADE,
                                verbose_name='Препод')

    class Meta:
        verbose_name = 'Группа'
        verbose_name_plural = 'Группы'

    def __str__(self):
        return f"{self.name} "


class Student(models.Model):
    name = models.CharField(max_length=255, verbose_name='Имя')
    last_name = models.CharField(max_length=255,
                                 verbose_name='Фамилия',
                                 null=True)
    phone = PhoneNumberField(null=False, default='-')
    age = models.IntegerField(validators=[MinValueValidator(1),
                                          MaxValueValidator(100)],
                              verbose_name='Возраст')
    group = models.ForeignKey(Group,
                              on_delete=models.CASCADE,
                              verbose_name='Группа')

    class Meta:
        verbose_name = 'Студент'
        verbose_name_plural = 'Студенты'

    def __str__(self):
        return f"{self.name}, {self.group}"
