from core.models import Group, Student   # Student, Teacher,

from django import forms


# class GroupCreateForm(forms.Form):
#     name = forms.CharField()
#     teacher = forms.ModelChoiceField(
#         queryset=TeacherModel.objects.all(),
#         widget=forms.widgets.RadioSelect()
#     )
#
#     def save(self):
#         track = GroupModel.objects.create(**self.cleaned_data)
#         return track


class GroupCreateForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = '__all__'
        widgets = {
            'teacher': forms.widgets.RadioSelect()
        }


# class StudentCreateForm(forms.Form):
#     name = forms.CharField()
#     age = forms.IntegerField(max_value=80,
#                              min_value=17
#                              )
#     group = forms.ModelChoiceField(queryset=Teacher.objects.all(),
#                                    widget=forms.widgets.CheckboxInput()
#                                    )


class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = '__all__'
