from core.views import CreateGroupView,\
                       CreateStudentView,\
                       CreateTeacherView,\
                       DeleteStudentView,\
                       DeleteTeacherView, \
                       GroupView,\
                       StudentView,\
                       TeacherView, \
                       UpdateStudentView,\
                       UpdateTeacherView

from django.urls import path


app_name = "core"

urlpatterns = [
    path('group/create/', CreateGroupView.as_view(),
         name='create-group'),
    path('teacher/create/', CreateTeacherView.as_view(),
         name='create-teacher'),
    path('teacher/list/', TeacherView.as_view(),
         name='teacher-list'),
    path('teacher/update/<int:pk>', UpdateTeacherView.as_view(),
         name='update-teacher'),
    path('student/create/', CreateStudentView.as_view(),
         name='create-student'),
    path('student/update/<int:pk>', UpdateStudentView.as_view(),
         name='update-student'),
    path('student/list', StudentView.as_view(), name='students-list'),
    path('student/delete/<int:pk>', DeleteStudentView.as_view(),
         name='delete-student'),
    path('teacher/delete/<int:pk>', DeleteTeacherView.as_view(),
         name='delete-teacher'),
    path('', GroupView.as_view(), name='home'),
]
